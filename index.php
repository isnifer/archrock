<?require_once'inc.php';?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>ArchRock</title>
    <meta name="description" content="Short page description"/>
    <meta name="keywords" content="first second third key word"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="assets/css/jquery.fancybox.css"/>
    <link rel="stylesheet" href="assets/css/style.min.css"/>
    <!--[if lt IE 9]>
    <script src="assets/js/libs/html5shiv-printshiv.js"></script>
    <![endif]-->
</head>

<body>
    <div class="page">
       
        <div class="page__wrapper g-clf">

            <!-- Шапка -->
            <header class="header g-clf">
                <div class="header__call">
                    <div class="header__call-number">
                        8 (4932) <strong>57-61-28</strong>
                    </div>
                    <a href="#call" class="header__call-btn btn fancybox">
                        заказать обратный звонок
                    </a>
                </div>
                <div class="header__towns">
                    <span class="header__town">Владимир</span>
                    <span class="header__town">Иваново</span>
                    <span class="header__town">Ярославль</span>
                    <span class="header__town">Кострома</span>
                </div>
                <div class="header__logo"></div>
                <div class="menu">
                    <a href="#" class="menu__item active">производство</a>
                    <a href="#" class="menu__item">монтаж</a>
                    <a href="#" class="menu__item">доставка</a>
                    <a href="#" class="menu__item">проектирование</a>
                </div>
            </header>

            <!-- Контент над панелью -->
            <div class="top-panel">
                <div class="top-panel__panels">
                    <div class="top-panel__tech">
                        <span class="top-panel__3d"><b>3D</b> панели</span> из гипса по итальянской технологии
                    </div>
                    <div class="top-panel__design">
                        Восхитительный дизайн стен для вашего интерьера
                    </div>
                    <div class="top-panel__cost">
                        <div class="top-panel__cost-text">всего за:</div>
                        <div class="top-panel__cost-value">4180</div>
                    </div>
                    <div class="top-panel__sale">
                        Получите
                        <span class="top-panel__sale-meters">
                            <span class="top-panel__sale-meters-value">2м</span><span class="top-panel__sale-meters-square">&sup2;</span>
                        </span>
                        панелей бесплатно
                    </div>
                    <div class="top-panel__value">
                        8360 руб. <span class="top-panel__value-meters">при заказе от 6 м&sup2;</span>
                    </div>
                </div>
                <form class="top-form" action=".?#top-sale" id="top-sale" method="post" name="sale">
                    <div class="top-form__fields">
                        <label class="top-form__label">
                            <input type="text" class="top-form__input" name="user_name" value="<?=@$GLOBALS['user_name']?>" placeholder="Ваше имя" required="true"/>
                        </label>
                        <label class="top-form__label">
                            <input type="text" class="top-form__input" name="user_phone" value="<?=@$GLOBALS['user_phone']?>" placeholder="Ваш телефон" required="true"/>
                        </label>
                        <span class="error"><?=@$GLOBALS['panel_free'];?></span>
                    </div>
                    <button class="top-form__submit btn">Получить скидку</button>
					<input type="hidden" value="panel_free" name="form_type"/>
                </form>
                <div class="pre-timer">
                    До конца акции осталось:
                </div>
                <div class="timer g-clf">
                    <div class="timer__item">
                        <div class="timer__value">32</div>
                        <div class="timer__title">Дни</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">14</div>
                        <div class="timer__title">Часы</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">19</div>
                        <div class="timer__title">Минуты</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">45</div>
                        <div class="timer__title">Секунды</div>
                    </div>
                </div>
            </div>

            <!-- Преимущества -->
            <div class="benefits">
                <div class="benefits__item benefits__item_1">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">НЕ ПРИТЯГИВАЕТ ПЫЛЬ</div>
                    <div class="benefits__text">
                        Гипс не накапливает <br/>
                        электростатических зарядов, <br/>
                        поэтому не притягивает лишней пыли
                    </div>
                </div>
                <div class="benefits__item benefits__item_2">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">ЭКСКЛЮЗИВНОСТЬ</div>
                    <div class="benefits__text">
                        Помещения с панелями ARCHROCK <br/>
                        приобретают неповторимый вид
                    </div>
                </div>
                <div class="benefits__item benefits__item_3">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">ЭКОЛОГИЧНОСТЬ</div>
                    <div class="benefits__text">
                        Гипс — экологически чистый <br/>
                        природный материал, <br/>
                        безвреден для здоровья
                    </div>
                </div>
                <div class="benefits__item benefits__item_4">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">ПРОЧНОСТЬ</div>
                    <div class="benefits__text">
                        Панели ARCHROCK не боятся <br/>
                        механических воздействий, <br/>
                        срок службы более 100 лет
                    </div>
                </div>
                <div class="benefits__item benefits__item_5">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">ОКРАСКА В ЛЮБОЙ ЦВЕТ</div>
                    <div class="benefits__text">
                        Вы можете окрасить панели в <br/> любые цвета
                    </div>
                </div>
                <div class="benefits__item benefits__item_6">
                    <i class="benefits__icon"></i>
                    <div class="benefits__title">ВЛАГОСТОЙКОСТЬ</div>
                    <div class="benefits__text">
                        Не деформируется под <br/>
                        воздействием пара и влажности
                    </div>
                </div>
            </div>

            <!-- Услуги -->
            <div class="services">
                <div class="services__item">Поставка от 3х дней</div>
                <div class="services__item">Скидки от объема</div>
                <div class="services__item">Монтаж панелей</div>
            </div>

            <!-- Продукты -->
            <div class="products">
                <div class="products__item g-clf">
                    <div class="products__photo">
                        <a class="products__photo-link fancybox" href="assets/img/products/archrock_crystal.png">
                            <img src="assets/img/products/archrock_crystal_small.png" alt=""/>
                        </a>
                    </div>
                    <div class="products__content">
                        <div class="products__title">
                            3D панель
                        </div>
                        <div class="products__header">
                            Crystal
                        </div>
                        <div class="products__footer">
                            <div class="products__details g-clf">
                                <div class="products__details-left">
                                    <div class="products__thickness">
                                        толщина 30мм
                                    </div>
                                    <img class="products__scheme" src="assets/img/products/archrock-28.png" alt=""/>
                                </div>
                                <div class="products__details-right">
                                    <img class="products__view" src="assets/img/products/model_crystal.png" alt=""/>
                                    <div class="products__download">
                                        <a class="products__download-link" href="https://yadi.sk/d/_BU4RV-EbRyLV" target="_blank">скачать 3D модель</a>
                                    </div>
                                </div>
                            </div>
                            <div class="products__cost g-clf">
                                <div class="products__cost-left">
                                    <div class="products__name">
                                        Crystal
                                    </div>
                                    <div class="products__new-value">
                                        4 180
                                    </div>
                                </div>
                                <div class="products__cost-right">
                                    <a class="products__btn btn fancybox" href="#product" data-product="Crystal">Заказать</a>
                                    <div class="products__or">или</div>
                                    <div class="products__consult">
                                        <a class="products__consult-link fancybox" href="#consulting">Консультация дизайнера</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products__item g-clf">
                    <div class="products__photo">
                        <a class="products__photo-link fancybox" href="assets/img/products/archrock_moon.png">
                            <img src="assets/img/products/archrock_moon_small.png" alt=""/>
                        </a>
                    </div>
                    <div class="products__content">
                        <div class="products__title">
                            3D панель
                        </div>
                        <div class="products__header">
                            Moon
                        </div>
                        <div class="products__footer">
                            <div class="products__details g-clf">
                                <div class="products__details-left">
                                    <div class="products__thickness">
                                        толщина 25мм
                                    </div>
                                    <img class="products__scheme" src="assets/img/products/archrock-27.png" alt=""/>
                                </div>
                                <div class="products__details-right">
                                    <img class="products__view" src="assets/img/products/model_moon.png" alt=""/>
                                    <div class="products__download">
                                        <a class="products__download-link" href="https://yadi.sk/d/_BU4RV-EbRyLV" target="_blank">
                                            скачать 3D модель
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="products__cost products__cost_sale g-clf">
                                <div class="products__cost-left">
                                    <div class="products__name">
                                        Moon
                                    </div>
                                    <div class="products__old-value">
                                        4 180
                                    </div>
                                    <div class="products__new-value">
                                        3 360
                                    </div>
                                    <div class="products__countdown">
                                        До конца акции остался: 1 день
                                    </div>
                                </div>
                                <div class="products__cost-right">
                                    <a class="products__btn btn fancybox" href="#product" data-product="Moon">Заказать</a>
                                    <div class="products__or">или</div>
                                    <div class="products__consult">
                                        <a class="products__consult-link fancybox" href="#consulting">Консультация дизайнера</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products__item g-clf">
                    <div class="products__photo">
                        <a class="products__photo-link fancybox" href="assets/img/products/archrock_kite.png">
                            <img src="assets/img/products/archrock_kite_small.png" alt=""/>
                        </a>
                    </div>
                    <div class="products__content">
                        <div class="products__title">
                            3D панель
                        </div>
                        <div class="products__header">
                            Kite
                        </div>
                        <div class="products__footer">
                            <div class="products__details g-clf">
                                <div class="products__details-left">
                                    <div class="products__thickness">
                                        толщина 25мм
                                    </div>
                                    <img class="products__scheme" src="assets/img/products/archrock-29.png" alt=""/>
                                </div>
                                <div class="products__details-right">
                                    <img class="products__view" src="assets/img/products/model_kite.png" alt=""/>
                                    <div class="products__download">
                                        <a class="products__download-link" href="https://yadi.sk/d/_BU4RV-EbRyLV" target="_blank">
                                            скачать 3D модель
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="products__cost products__cost_sale g-clf">
                                <div class="products__cost-left">
                                    <div class="products__name">
                                        Kite
                                    </div>
                                    <div class="products__old-value">
                                        4 180
                                    </div>
                                    <div class="products__new-value">
                                        3 690
                                    </div>
                                    <div class="products__countdown">
                                        До конца акции остался: 1 день
                                    </div>
                                </div>
                                <div class="products__cost-right">
                                    <a class="products__btn btn fancybox" href="#product" data-product="Kite">Заказать</a>
                                    <div class="products__or">или</div>
                                    <div class="products__consult">
                                        <a class="products__consult-link fancybox" href="#consulting">Консультация дизайнера</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products__item g-clf">
                    <div class="products__photo">
                        <a class="products__photo-link fancybox" href="assets/img/products/archrock_rize.png">
                            <img src="assets/img/products/archrock_rize_small.png" alt=""/>
                        </a>
                    </div>
                    <div class="products__content">
                        <div class="products__title">
                            3D панель
                        </div>
                        <div class="products__header">
                            Rize
                        </div>
                        <div class="products__footer">
                            <div class="products__details g-clf">
                                <div class="products__details-left">
                                    <div class="products__thickness">
                                        толщина 25мм
                                    </div>
                                    <img class="products__scheme" src="assets/img/products/archrock-26.png" alt=""/>
                                </div>
                                <div class="products__details-right">
                                    <img class="products__view" src="assets/img/products/model_rize.png" alt=""/>
                                    <div class="products__download">
                                        <a class="products__download-link" href="https://yadi.sk/d/_BU4RV-EbRyLV" target="_blank">
                                            скачать 3D модель
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="products__cost g-clf">
                                <div class="products__cost-left">
                                    <div class="products__name">
                                        Rize
                                    </div>
                                    <div class="products__new-value">
                                        4 180
                                    </div>
                                </div>
                                <div class="products__cost-right">
                                    <a class="products__btn btn fancybox" href="#product" data-product="Rize">Заказать</a>
                                    <div class="products__or">или</div>
                                    <div class="products__consult">
                                        <a class="products__consult-link fancybox" href="#consulting">Консультация дизайнера</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products__item g-clf">
                    <div class="products__photo">
                        <a class="products__photo-link fancybox" href="assets/img/products/archrock_stels.png">
                            <img src="assets/img/products/archrock_stels_small.png" alt=""/>
                        </a>
                    </div>
                    <div class="products__content">
                        <div class="products__title">
                            3D панель
                        </div>
                        <div class="products__header">
                            Stels
                        </div>
                        <div class="products__footer">
                            <div class="products__details g-clf">
                                <div class="products__details-left">
                                    <div class="products__thickness">
                                        высота 55мм
                                    </div>
                                    <img class="products__scheme" src="assets/img/products/archrock-25.png" alt=""/>
                                </div>
                                <div class="products__details-right">
                                    <img class="products__view" src="assets/img/products/model_stels.png" alt=""/>
                                    <div class="products__download">
                                        <a class="products__download-link" href="https://yadi.sk/d/_BU4RV-EbRyLV" target="_blank">
                                            скачать 3D модель
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="products__cost g-clf">
                                <div class="products__cost-left">
                                    <div class="products__name">
                                        Stels
                                    </div>
                                    <div class="products__new-value">
                                        4 180
                                    </div>
                                </div>
                                <div class="products__cost-right">
                                    <a class="products__btn btn fancybox" href="#product" data-product="Stels">Заказать</a>
                                    <div class="products__or">или</div>
                                    <div class="products__consult">
                                        <a class="products__consult-link fancybox" href="#consulting">Консультация дизайнера</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Фиолетовый блок -->
        <div class="violent" id="violent">
            <div class="top-panel">
                <div class="top-panel__panels">
                    <div class="top-panel__sale">
                        Получите
                        <span class="top-panel__sale-meters">
                            <span class="top-panel__sale-meters-value">2м</span><span class="top-panel__sale-meters-square">&sup2;</span>
                        </span>
                        панелей бесплатно
                    </div>
                    <div class="top-panel__value">
                        8360 руб. <span class="top-panel__value-meters">при заказе от 6 м&sup2;</span>
                    </div>
                </div>
                <form class="top-form" action=".?#violent" method="post" name="sale">
                    <div class="top-form__fields">
                        <label class="top-form__label">
                            <input type="text" class="top-form__input" value="<?=@$GLOBALS['user_name']?>" name="user_name" placeholder="Ваше имя" required="true" />
                        </label>
                        <label class="top-form__label">
                            <input type="text" class="top-form__input" value="<?=@$GLOBALS['user_phone']?>" name="user_phone" placeholder="Ваш телефон" required="true" />
                        </label>
                        <span class="error"><?=@$GLOBALS['panel_free_two'];?></span>
                    </div>
                    <button class="top-form__submit btn">Получить скидку</button>
					<input type="hidden" value="panel_free_two" name="form_type"/>
                </form>
                <div class="pre-timer">
                    До конца акции осталось:
                </div>
                <div class="timer g-clf">
                    <div class="timer__item">
                        <div class="timer__value">32</div>
                        <div class="timer__title">Дни</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">14</div>
                        <div class="timer__title">Часы</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">19</div>
                        <div class="timer__title">Минуты</div>
                    </div>
                    <div class="timer__item">
                        <div class="timer__value">45</div>
                        <div class="timer__title">Секунды</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Интерьер -->
        <div class="page__wrapper">
            <div class="interior" id="interior">
                <div class="interior__header">ДОБАВЬТЕ ФОТО ВАШЕГО ИНТЕРЬЕРА</div>
                <div class="interior__subheader">и получите врисовку с нашими панелями </div>
                <div class="interior__free">
                    <span class="interior__free-text">бесплатно</span>
                </div>
                <div class="interior__photo-left">
                    <img src="assets/img/interior/photo-left.png" alt=""/>
                </div>
                <form action=".?#interior" class="interior__form form" method="post" enctype="multipart/form-data" name="interior">
                    <label class="form__label">
                        <span class="interior__file">
                            <i class="interior__cross"></i>
                            Добавить фотографию
                        </span>
                        <input type="file" accept="image/*" name="user_file" class="interior__file-input"  required="true" />
                    </label>
                    <label class="interior__label">
                        <input type="text" name="user_name" value="<?=@$GLOBALS['user_name']?>" class="input" placeholder="Ваше Имя" required="true" />
                    </label>
                    <label class="interior__label">
                        <input type="text" name="user_email" value="<?=@$GLOBALS['user_email']?>" class="input" placeholder="Ваш e-mail" required="true" />
                    </label>
                    <label class="form__label">
                        <textarea class="textarea" name="user_text" id="" cols="30" rows="10" placeholder="Какую модель желаете?         Примечания дизайнеру?"><?=@$GLOBALS['user_text']?></textarea>
                    </label>
                    <span class="error"><?=@$GLOBALS['add_photo'];?></span>
                    <button type="submit" class="interior__submit btn">отправить</button>
					<input type="hidden" name="form_type" value="add_photo" />
                </form>
                <div class="interior__photo-right">
                    <img src="assets/img/interior/photo-right.png" alt=""/>
                </div>
            </div>
        </div>

        <!-- Дизайн-проект -->
        <div class="design">
            <div class="page__wrapper">
                <div class="design__value">
                    от <b>800</b> <i class="design__value-rub"></i>
                </div>
                <div class="design__header">
                    НУЖЕН ДИЗАЙН - ПРОЕКТ <br/>
                    ВСЕГО ИНТЕРЬЕРА?
                </div>
                <div class="design__subheader">
                    проектирование жилых и общественных интерьеров
                </div>
                <div class="design__list">
                    <div class="design__item design__item_1">
                        <i class="design__icon"></i>
                        <div class="design__text">Проект «Под ключ»</div>
                    </div>
                    <div class="design__item design__item_2">
                        <i class="design__icon"></i>
                        <div class="design__text">Бесплатный авторский надзор</div>
                    </div>
                    <div class="design__item design__item_3">
                        <i class="design__icon"></i>
                        <div class="design__text">Гарантия 365 дней <br/> на строительные услуги</div>
                    </div>
                    <div class="design__item design__item_4">
                        <i class="design__icon"></i>
                        <div class="design__text">Скидки на материалы <br/> и оборудование у наших партнеров</div>
                    </div>
                </div>
                <div class="design__request-header">Наши работы:</div>
                <div class="design__request g-clf">
                    <div class="design__slider slider">
                        <img src="assets/img/slider/slide1.jpg" alt=""/>
                        <img src="assets/img/slider/slide4.jpg" alt=""/>
                        <img src="assets/img/slider/slide5.jpg" alt=""/>
                        <img src="assets/img/slider/slide6.jpg" alt=""/>
                        <img src="assets/img/slider/slide7.jpg" alt=""/>
                        <img src="assets/img/slider/slide9.jpg" alt=""/>
                        <img src="assets/img/slider/slide16.jpg" alt=""/>
                        <img src="assets/img/slider/slide19.jpg" alt=""/>
                        <img src="assets/img/slider/slide34.jpg" alt=""/>
                        <img src="assets/img/slider/slide36.jpg" alt=""/>
                        <img src="assets/img/slider/slide40.jpg" alt=""/>
                        <img src="assets/img/slider/slide42.jpg" alt=""/>
                    </div>
                    <form class="design__form" id="design_form" action=".?#design_form" method="post" name="design">
                        <span class="design__form-header">Заказать проект</span>
                        <div class="design__form-wrapper">
                            <label class="design__form-label">
                                <input class="design__input input" type="text" name="user_name" value="<?=@$GLOBALS['user_name']?>" placeholder="Ваше имя" required="true" />
                            </label>
                            <label class="design__form-label">
                                <input class="design__input input" type="text" name="user_phone" value="<?=@$GLOBALS['user_phone']?>" placeholder="Ваш телефон" required="true" />
                            </label>
                            <label class="design__form-label">
                                <input class="design__input input" type="email" name="user_email" value="<?=@$GLOBALS['user_email']?>" placeholder="Ваш e-mail" required="true" />
                            </label>
                            <button class="design__submit btn" type="submit">отправить</button>
                            <input type="hidden" value="order_project" name="form_type"/>
                            <span class="error"><?=@$GLOBALS['order_project'];?></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Монтаж -->
        <div class="edit">
            <div class="page__wrapper g-clf">
                <div class="edit__main-header">
                    <b class="edit__header-big">Монтаж</b> быстро и просто
                </div>
                <div class="edit__part g-clf">
                    <div class="edit__header">1. Подготовка</div>
                    <div class="edit__item edit__item_1">
                        <div class="edit__title">Аклиматизация</div>
                        <div class="edit__text">
                            Оставьте панели в помещении на 48 часов на акклиматизацию – в течение этого времени 3d панели приобретут необходимую влажность и температуру, что гарантирует неизменность их размеров в процессе эксплуатации.
                        </div>
                    </div>
                    <div class="edit__item edit__item_2">
                        <div class="edit__title">Выравнивание стен</div>
                        <div class="edit__text">Поверхность стены должна быть ровной (перепады до 2 мм/м), чистой, сухой и шершавой. Для лучшей адгезии, рекомендуем покрыть кварц-грунтом поверхность стены и панели. </div>
                    </div>
                    <div class="edit__item edit__item_3">
                        <div class="edit__title">Раскладка</div>
                        <div class="edit__text">Затем нужно определиться с раскладкой и расположением рисунка панелей на стене, обозначить части панелей, которые уйдут на подрезку в случае необходимости, а также обозначить отступы для плинтуса, потолка и т. п. По нижнему краю необходимо прикрепить профиль, на который будет опираться нижняя часть панели.</div>
                    </div>
                </div>
                <div class="g-clf">
                    <div class="edit__part edit__part_half">
                        <div class="edit__header">2. Монтаж</div>
                        <div class="edit__text">Клей наносится гребенкой с большим размером зубьев (10-12 мм) и панель плотно прижимается к стене. В местах стыковки панелей шов надо подгонять максимально четко. Для монтажа панелей можно использовать любой гипсовый строительный клей, например, knauf Perlfix. Чем более продолжительное время застывания клея, тем проще добиться более качественного результата.</div>
                    </div>
                    <div class="edit__part edit__part_half">
                        <div class="edit__header">3. Покраска</div>
                        <div class="edit__text">После этого, поверхность панелей следует загрунтовать
                            и нанести финишную отделку.  Нанести  любую краску кистью,
                            валиком или краскопультом. Можно использовать фактурную
                            штукатурку.</div>
                        <a class="edit__btn btn fancybox" href="#consulting">Консультация по установке</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="page__wrapper">

            <!-- Особые условия -->
            <div class="special">
                <div class="special__benefits">
                    <span class="special__benefits-text">для вас особые</span>
                    <a class="special__btn btn fancybox" href="#partnership">Условия сотрудничества</a>
                </div>
                <div class="special__title">
                    ВЫ  ДИЗАЙНЕР? <br/>
                    СТРОИТЕЛЬНЫЙ МАГАЗИН? <br/>
                    ИЛИ СТУДИЯ?
                </div>
            </div>

            <!-- Подвал -->
            <footer class="footer g-clf">
                <div class="footer__call">
                    <div class="footer__email">
                        <a class="footer__email-link" href="mailto:mail@archrock.ru">
                            mail@archrock.ru
                        </a>
                    </div>
                    <div class="footer__call-number">
                        8 (4932) <strong>57-61-28</strong>
                    </div>
                    <a href="#call" class="footer__call-btn btn fancybox">
                        заказать обратный звонок
                    </a>
                    <div class="footer__recall">
                        перезвоним, проконсультируем
                    </div>
                </div>
                <div class="footer__logo">
                    <div class="footer__towns">
                        <span class="footer__town">Владимир</span>
                        <span class="footer__town">Иваново</span>
                        <span class="footer__town">Ярославль</span>
                        <span class="footer__town">Кострома</span>
                    </div>
                </div>
                <div class="menu g-clf">
                    <a href="#" class="menu__item active">производство</a>
                    <a href="#" class="menu__item">монтаж</a>
                    <a href="#" class="menu__item">доставка</a>
                    <a href="#" class="menu__item">проектирование</a>
                </div>
            </footer>
        </div>

        <div class="popup">
            <div class="popup__item" id="call">
                <div class="popup__content g-clf">
                    <div class="popup__title">
                        Заказать обратный звонок
                    </div>
                    <form action=".?#call" method="post" name="call" class="popup__form">
                        <label class="popup__label">
                            <input class="popup__input input" name="user_name" placeholder="Ваше имя *" value="<?=@$GLOBALS['user_name']?>" required="true" type="text"/>
                        </label>
                        <label class="popup__label">
                            <input class="popup__input input" name="user_phone" placeholder="Ваше телефон *" value="<?=@$GLOBALS['user_phone']?>" required="true" type="text"/>
                        </label>
                        <button type="submit" class="popup__submit btn">Отправить</button>
						<input type="hidden" value="popup_callback" name="form_type"/>
                    </form>
                    <div class="popup__info">
                        перезвоним, проконсультируем
                    </div>
                    <span class="error"><?=@$GLOBALS['popup_callback'];?></span>
                </div>
            </div>
            <div class="popup__item" id="product">
                <div class="popup__content g-clf">
                    <div class="popup__title">
                        Заказать панели
                    </div>
                    <form action=".?#product" method="post" name="product" class="popup__form">
                        <label class="popup__label">
                            <input class="popup__input input" name="user_name" placeholder="Ваше имя *" value="<?=@$GLOBALS['user_name']?>" required="true" type="text"/>
                        </label>
                        <label class="popup__label">
                            <input class="popup__input input" name="user_phone" placeholder="Ваше телефон *" value="<?=@$GLOBALS['user_phone']?>" required="true" type="text"/>
                        </label>
                        <button type="submit" class="popup__submit btn">Отправить</button>
						<input type="hidden" value="popup_product" name="form_type"/>
						<input type="hidden" value="" name="user_product" id="product_value"/>
                    </form>
                    <div class="popup__info">
                        мы вам скоро перезвоним
                    </div>
                    <span class="error"><?=@$GLOBALS['popup_product'];?></span>
                </div>
            </div>
            <div class="popup__item" id="consulting">
                <div class="popup__content g-clf">
                    <div class="popup__title">
                        Заказать консультацию
                    </div>
                    <form action=".?#consulting" method="post" name="consulting" class="popup__form">
                        <label class="popup__label">
                            <input class="popup__input input" name="user_name" placeholder="Ваше имя *" value="<?=@$GLOBALS['user_name']?>" required="true" type="text"/>
                        </label>
                        <label class="popup__label">
                            <input class="popup__input input" name="user_phone" placeholder="Ваше телефон *" value="<?=@$GLOBALS['user_phone']?>" required="true" type="text"/>
                        </label>
                        <button type="submit" class="popup__submit btn">Отправить</button>
						<input type="hidden" value="popup_consulting" name="form_type"/>
                    </form>
                    <div class="popup__info">
                        с вами свяжется дизайнер
                    </div>
                    <span class="error"><?=@$GLOBALS['popup_consulting'];?></span>
                </div>
            </div>
            <div class="popup__item" id="partnership">
                <div class="popup__content g-clf">
                    <div class="popup__title">
                        Условия сотрудничества
                    </div>
                    <form action=".?#partnership" method="post" name="partnership" class="popup__form">
                        <label class="popup__label">
                            <input class="popup__input input" name="user_name" placeholder="Ваше имя *" value="<?=@$GLOBALS['user_name']?>" required="true" type="text"/>
                        </label>
                        <label class="popup__label">
                            <input class="popup__input input" name="user_phone" placeholder="Ваше телефон *" value="<?=@$GLOBALS['user_phone']?>" required="true" type="text"/>
                        </label>
                        <label class="popup__label">
                            <input class="popup__input input" name="user_email" placeholder="Ваше email *" value="<?=@$GLOBALS['user_email']?>" required="true" type="email"/>
                        </label>
                        <button type="submit" class="popup__submit btn">Отправить</button>
						<input type="hidden" value="popup_partnership" name="form_type"/>
                    </form>
                    <div class="popup__info popup__info_left">
                        с вами свяжется менеджер <br/>
                        и отправит условия сотрудничества <br/>
                        на почту
                    </div>
                    <span class="error"><?=@$GLOBALS['popup_partnership'];?></span>
                </div>
            </div>
        </div>

    </div>

    <script src="assets/js/libs/jquery-1.11.0.min.js"></script>
    <script src="assets/js/plugins/jquery.slides.min.js"></script>
    <script src="assets/js/plugins/jquery.cookie.js"></script>
    <script src="assets/js/plugins/jquery.fancybox.pack.js"></script>
    <script src="src/js/common.js"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter26173458 = new Ya.Metrika({id:26173458,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/26173458" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>
