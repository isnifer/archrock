<?
	$type = isset($_REQUEST['form_type']) ? $_REQUEST['form_type'] : '';
	if ($type)
		{
		$ctrl = new FormProcessor($type);
		if ($ctrl->isValid())
			{
			$method = $type.'_Action';
			if (method_exists($ctrl,$method))
				{
				if ($ctrl->$method())
					{
					ob_clean();
					header('location:thankyou.html');
					exit;
					}
				}
			}
		else
			{
			$GLOBALS['errors'] = $ctrl->errors;
			}
		}

	class FormProcessor
		{
		private $type,$values;
		public $errors = array();
		static $tpl_folder = 'mail_templates/';
		static $root_mail = 'mail@archrock.ru';
		
		// описание полей форм с типами валидаторов
		private $fieldsbytype = array(
			'add_photo' => array('user_name'=>'text','user_email'=>'email','user_text'=>'text','user_file'=>'file'),
			'panel_free' => array('user_name'=>'text','user_phone'=>'text'),
			'panel_free_two' => array('user_name'=>'text','user_phone'=>'text'),
			'popup_callback' => array('user_name'=>'text','user_phone'=>'text'),
			'popup_product' => array('user_name'=>'text','user_phone'=>'text','user_product'=>'text'),
			'popup_consulting' => array('user_name'=>'text','user_phone'=>'text'),
			'popup_partnership' => array('user_name'=>'text','user_phone'=>'text','user_email'=>'text'),
			'order_project' => array('user_name'=>'text','user_phone'=>'text','user_email'=>'text'),
			);
		// шаблоны тем писем
		private $subj_tpl = array(
			'add_photo' => 'Фото интерьера',
			'panel_free' => 'Заявка на получение панелей бесплатно',
			'popup_callback' => 'Заявка на обратный звонок',
			'popup_product' => 'Заявка на получение панели',
			'popup_consulting' => 'Заявка на получение консультации',
			'popup_partnership' => 'Запрос о сотрудничестве',
			'order_project' => 'Заказ проекта',
			);

		private $MapFields = array(
			'user_name' => 'Ваше имя',
			'user_email' => 'E-mail',
			'user_phone' => 'Телефон',
			'user_file' => 'Фотография',
			'user_text' => 'Примечания дизайнеру',
			);

		function __construct($type)
			{
			$this->type = $type;
			}

		// валидация
		function isValid()
			{
			foreach ($this->fieldsbytype[$this->type] as $field=>$type)
				{
				$value = ($type == 'file') ? $_FILES[$field] : trim($_REQUEST[$field]);
				switch ($type)
					{
					case 'text':
					if (empty($value))
						{
						$this->errors[] = $field;
						}
					break;
					
					case 'email':
					if (!preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $value) || !$value)
						{
						$this->errors[] = $field;
						}
					break;

					case 'file':
					if (($value['error']>0) || !file_exists($value['tmp_name']))
						{
						$this->errors[] = $field;
						}
					break;
					}
				$GLOBALS[$field] = $this->values[$field] = $value;
				}
			$form_errors = array();
			foreach ($this->errors as $prop)
				{
				$form_errors[] = $this->MapFields[$prop];
				}
			if (count($form_errors))
				{
				$GLOBALS[$this->type] = 'Вы допустили ошибки в следующих полях: '.implode(', ',$form_errors);
				}
			return count($this->errors) ? false : true;
			}

		function Template()
			{
			$file = self::$tpl_folder.$this->type.'.tpl';
			if (file_exists($file))
				{
				$tpl = file_get_contents($file);
				foreach ($this->values as $prop=>$value)
					{
					if (is_string($value))
						{
						$tpl = str_replace('{'.$prop.'}',$value,$tpl);
						}
					}
				return $tpl;
				}
			}

		private function SendMail($text,$filename)
			{
			$to = $from = self::$root_mail;
			$subj = $this->subj_tpl[$this->type];
			$file_desc = fopen($filename['tmp_name'],"rb"); 
			$unix_time        = strtoupper(uniqid(time())); 
			$head      = "From: Сайт Archrock <$from\>n"; 
			$head     .= "To: $to\n"; 
			$head     .= "Subject: $subj\n"; 
			$head     .= "X-Mailer: PHPMail Tool\n"; 
			$head     .= "Reply-To: $from\n"; 
			$head     .= "Mime-Version: 1.0\n"; 
			$head     .= "Content-Type:multipart/mixed;"; 
			$head     .= "boundary=\"----------".$unix_time."\"\n\n"; 
			$mhead       = "------------".$unix_time."\nContent-Type:text/html;\n"; 
			$mhead      .= "Content-Transfer-Encoding: 8bit\n\n$text\n\n"; 
			$mhead      .= "------------".$unix_time."\n"; 
			$mhead      .= "Content-Type: application/octet-stream;"; 
			$mhead      .= "name=\"".basename($filename['name'])."\"\n"; 
			$mhead      .= "Content-Transfer-Encoding:base64\n"; 
			$mhead      .= "Content-Disposition:attachment;"; 
			$mhead      .= "filename=\"".basename($filename['name'])."\"\n\n"; 
			$mhead      .= chunk_split(base64_encode(fread($file_desc,filesize($filename['tmp_name']))))."\n"; 
			return @mail($to,$subj,$mhead,$head); 
			}

		private function SimpleSendMail($text)
			{
			$to = $from = self::$root_mail;
			$subject = $this->subj_tpl[$this->type];
			$headers   = array();
			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=utf-8';
			$headers[] = 'From: Сайт Archrock <'.$from.'>';
			$headers[] = 'Reply-To: '.$from; 
			$headers[] = 'Subject: '.$subject;
			$headers[] = 'X-Mailer: PHP/'.phpversion();
			return @mail($from, $subject, $text, implode("\r\n", $headers));
			}

		// add_photo form
		function add_photo_Action()
			{
			return (int)$this->SendMail($this->Template(),$this->values['user_file']);
			}

		// panel_free
		function panel_free_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}

		function panel_free_two_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}

		function popup_callback_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}
		function popup_product_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}
		function popup_consulting_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}
		function popup_partnership_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}

		function order_project_Action()
			{
			return (int)$this->SimpleSendMail($this->Template());
			}
		}
?>