;(function (window, document, $, undefined) {

    $('.slider').slidesjs({
        width: 554,
        height: 309,
        navigation: {
            active: true
        },
        pagination: {
            active: false
        }
    });

    $('.fancybox').fancybox({
        padding: 0,
        scrolling: 'hidden',
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    var title = $('#product').find('.popup__title'),
        productValue = $('#product_value');
    $('.products__btn').on('click', function (e) {
        var productName = $(this).attr('data-product');
        title.html('Заказать панели ' + productName);
        productValue[0].value = productName;
    });

    // Timer set
    (function () {

        var date = new Date(),
            months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
            day = $.cookie('date') || date.getDate() + 2,
            month = (function () {
                var currentMonth = date.getMonth();

                if (currentMonth != 11) {
                    if (months[currentMonth + 1] < day) {
                        currentMonth += 2;
                    } else {
                        currentMonth += 1;
                    }
                } else {
                    if (months[11] < day) {
                        currentMonth = 1;
                    }
                }
                return (currentMonth < 10) ? '0' + currentMonth : currentMonth;
            }()),
            time = {
                hours: (date.getHours() < 10) ? '0' + date.getHours() : date.getHours(),
                minutes: (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes(),
                seconds: (date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds()
            },
            year = date.getFullYear(),
            rightDate,
            timerUrl;

        if (!$.cookie('date') && !$.cookie('time')) {
            $.cookie('date', day, {expires: 2});
            $.cookie('time', time.hours + ':' + time.minutes + ':' + time.seconds, {expires: 2});
        }

        rightDate = year + '-' + month + '-' + $.cookie('date');
        timerUrl = 'countdown.php?timezone=Europe/Moscow&countto=' + rightDate + ' ' + $.cookie('time');

        var timer = $('<script />').attr('src', timerUrl);

        $('body').append(timer);

    }());

} (window, window.document, window.jQuery));